<?php

namespace App\Controller;

use App\Feed\ItemTypes;
use App\Feed\XmlFeedReader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductController
{
    private $productFeedUrl;

    public function __construct(string $productFeedUrl)
    {
        $this->productFeedUrl = $productFeedUrl;
    }

    public function productFeedAction(Request $request, XmlFeedReader $itemReader)
    {
        $limit = $request->get('limit', 0);

        return new StreamedResponse(function () use ($itemReader, $limit) {
            $items = $itemReader->getItemsFromXml($this->productFeedUrl, ItemTypes::PRODUCT ,$limit);
            foreach ($items as $product) {
                echo json_encode($product);

                flush();
            }
        });
    }
}
