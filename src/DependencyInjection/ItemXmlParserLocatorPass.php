<?php

namespace App\DependencyInjection;

use App\Feed\ItemXmlParserInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ItemXmlParserLocatorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $taggedServices = $container->findTaggedServiceIds('item.xml.parser.locator');
        $itemXmlParserLocator = $container->register('item.xml.parser.locator')->addTag('container.service_locator');

        foreach ($taggedServices as $id => $attributes) {
            $interface = ItemXmlParserInterface::class;
            if (!in_array($interface, class_implements($id))) {
                throw new \Exception("Tagged definitions should rely on classes implementing $interface");
            }

            $itemXmlParserLocator->addArgument([
                $id::getTagName() => new Reference($id)
            ]);
        }
    }

}
