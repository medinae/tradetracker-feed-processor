<?php

namespace App\Feed;

class ProductXmlParser implements ItemXmlParserInterface
{
    const FLAT_ATTRIBUTES = ['productID', 'name', 'description', 'productURL', 'imageURL'];

    public function parse(\DOMNode $node): array
    {
        $product = [];

        foreach ($node->childNodes as $node) {
            if (in_array($node->nodeName, self::FLAT_ATTRIBUTES)) {
                $product[$node->nodeName] = $node->nodeValue;
            } elseif ($node->nodeName === 'price') {
                $product['price'] = $node->nodeValue;
                $product['price_currency'] = $node->attributes->getNamedItem('currency')->nodeValue;
            } elseif ($node->nodeName === 'categories') {
                $product['categories'] = [];
                /** @var \DOMNode $category */
                foreach ($node->childNodes as $category) {
                    if ($category->nodeName === 'category') {
                        $product['categories'][] = $category->nodeValue;
                    }
                }
            };
        }

        return $product;

    }

    public function supports(string $name): bool
    {
        return static::getTagName() === $name;
    }

    public static function getTagName(): string
    {
        return ItemTypes::PRODUCT;
    }
}
