<?php

namespace App\Feed;

interface ItemXmlParserInterface
{
    public function parse(\DOMNode $node): array;
    public function supports(string $name): bool;
    public static function getTagName(): string;
}
