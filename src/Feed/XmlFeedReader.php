<?php

namespace App\Feed;

use Psr\Container\ContainerInterface;

class XmlFeedReader
{
    private $itemParsersLocator;

    public function __construct(ContainerInterface $itemParsersLocator)
    {
        $this->itemParsersLocator = $itemParsersLocator;
    }

    public function getItemsFromXml(string $url, string $itemType, int $limit = 0): iterable
    {
        $parser = $this->resolveItemParser($itemType);
        $reader = new \XMLReader();

        if (!$reader->open($url)) {
            throw new \Exception("Impossible to read xml feed located at $url");
        }

        $index = 0;

        // we move to the first searched node
        while ($reader->read() && !$parser->supports($reader->name)) {
        }

        while ($parser->supports($reader->name)) {
            if ($limit !== 0 && $limit > $index) {
                break;
            }

            yield $parser->parse($reader->expand());

            $reader->next($parser->getTagName());
            $index++;
        }
    }

    private function resolveItemParser(string $itemType): ItemXmlParserInterface
    {
        if (!$this->itemParsersLocator->has($itemType)) {
            throw new \Exception("Parser related to tag $itemType does not exist");
        }

        return $this->itemParsersLocator->get($itemType);
    }
}
